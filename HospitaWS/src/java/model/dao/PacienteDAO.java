/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import model.pojos.Paciente;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author matt
 */
public class PacienteDAO {
    public Paciente getPaciente(String documento){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session session = sf.openSession();
        Query query = session.createQuery("FROM Paciente WHERE documento = '"+documento+"'");
        Paciente p = (Paciente) query.uniqueResult();
        session.close();
        return p;
    }
}
