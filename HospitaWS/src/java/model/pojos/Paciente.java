package model.pojos;
// Generated 02/05/2016 12:59:43 AM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Paciente generated by hbm2java
 */
public class Paciente  implements java.io.Serializable {


     private long id;
     private String documento;
     private String nombre;
     private String apellido;
     private String direccion;
     private String telefono;
     private Date fechaRegistro;
     private HistoriaClinica historiaClinica;

    public Paciente() {
    }

	
    public Paciente(long id) {
        this.id = id;
    }
    public Paciente(long id, String documento, String nombre, String apellido, 
            String direccion, String telefono, Date fechaRegistro, 
            HistoriaClinica historiaClinicas) {
       this.id = id;
       this.documento = documento;
       this.nombre = nombre;
       this.apellido = apellido;
       this.direccion = direccion;
       this.telefono = telefono;
       this.fechaRegistro = fechaRegistro;
       this.historiaClinica = historiaClinica;
    }
   
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public String getDocumento() {
        return this.documento;
    }
    
    public void setDocumento(String documento) {
        this.documento = documento;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return this.apellido;
    }
    
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getDireccion() {
        return this.direccion;
    }
    
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public String getTelefono() {
        return this.telefono;
    }
    
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }
    
    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    public HistoriaClinica getHistoriaClinicas() {
        return this.historiaClinica;
    }
    
    public void setHistoriaClinicas(HistoriaClinica historiaClinica) {
        this.historiaClinica = historiaClinica;
    }

}


