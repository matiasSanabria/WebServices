/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.webservices;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import model.dao.HistoriaClinicaDAO;
import model.dao.PacienteDAO;

/**
 *
 * @author matt
 */
@WebService(serviceName = "ObtenerHistoriaClinica")
public class ObtenerHistoriaClinica {

    /**
     * Web service operation
     * @param documento
     * @return 
     */
    @WebMethod(operationName = "obtenerHistoriaClinica")
    public String obtenerHistoriaClinica(@WebParam(name = "documento") String documento) {
        HistoriaClinicaDAO hcDAO = new HistoriaClinicaDAO();
        return hcDAO.getHistoriaClinica(documento);
    }
}
