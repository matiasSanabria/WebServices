/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.webservices;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import model.dao.PacienteDAO;
import model.pojos.Paciente;

/**
 *
 * @author matt
 */
@WebService(serviceName = "ObtenerPacienteWS")
public class ObtenerPacienteWS {

    /**
     * Web service operation
     * @param documento
     * @return 
     */
    @WebMethod(operationName = "obtenerPaciente")
    public String obtenerPaciente(@WebParam(name = "documento") String documento) {
        PacienteDAO pacDAO = new PacienteDAO();
        return pacDAO.getPaciente(documento).getNombre();
    }
}
